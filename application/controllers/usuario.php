<?php
class Usuario extends CI_Controller{
    public function get_usuarios(){
        $this->load->model('usuario_model');
        echo json_encode($this->usuario_model->get());
    }
    
    public function short_insert(){
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->model('usuario_model');
        echo json_encode($this->usuario_model->short_insert());
    }
    
    public function login(){
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->helper('url');
        $this->load->model('usuario_model');
        $user = null;
        if($this->usuario_model->get_login()){
            $user = $this->usuario_model->get_login()[0];
        }        
        if($user){
            $this->session->set_userdata('usuario', $user);
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }
    }
    
    public function logueo(){
        $this->load->helper('url');
        $this->load->view('usuario_inicio');
    }
    
    public function logout(){
        $this->load->library('session');
        $this->load->helper('url');
        $this->session->unset_userdata('usuario');
        $this->session->unset_userdata('sesion');
        redirect('evento');
    }
    
    public function participar(){
        $this->load->library('session');
        $this->load->helper('url');
        if($this->session->userdata('usuario')){
            $idUsuario = $this->session->userdata('usuario')['idUsuario'];
            $this->load->model('participacion_model');
            echo json_encode($this->participacion_model->insert($idUsuario));
        }else{
            echo json_encode('false');
        }
    }
}
?>