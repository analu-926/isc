<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset='utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <title>Sistemas</title>
    <meta name="description" content="Página oficial de la Escuela de Ingeniería de Sistemas - USAT">
    <meta name="author" content="AnaLu Carranza">
     <!-- CSS Code -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.css"/>
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>css/main.css'/>
    <!---link rel="stylesheet" href="<?php echo base_url();?>css/style.css"--->
</head>
<body>
    <header>
        <nav class='navbar navbar-default navbar-fixed-top navbar-inverse' role='navigation'>
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#menu-principal">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class='navbar-brand' href='<?php echo base_url();?>'>ISC</a>
                </div>
                <div class="collapse navbar-collapse" id='menu-principal'>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href='evento'>Eventos</a></li>
                        <li><a href='#'>Escuela</a></li>
                        <li><a href='#'>Foro</a></li>
                        <li><a href='#'>USAT</a></li>
                        <li><a href='#'>Contáctanos</a></li>
                    </ul>
                    <ul class='nav navbar-nav navbar-right'>
                        <li>
                            <?php if($sesion == 'true' && $usuario['idTipo'] == 2){?>
                            <button type='button' class='btn btn-primary navbar-btn' id='btnAgregar'>Agregar</button>
                            <?php }?>
                        </li>
                        <li>
                            <?php if($sesion == 'false') {?>
                            <a href='<?php echo base_url();?>usuario/logueo'>Iniciar sesión</a>
                            <?php }else{?>
                            <a href='<?php echo base_url();?>usuario/logout'><?php echo $usuario['correo'];?>[Cerrar]</a>
                            <?php } ?>
                        </li>
                    </ul>
                </div>   
            </div>  
        </nav> 
    </header>
    <section class="container">
        <div class="jumbotron">
            <h1><center>Conoce todos nuestros eventos</center></h1>
            <h3><center>¡Completamente GRATUITOS! </center></h3>
        </div>
        <div class="row" id='lstEventos'>            
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class='row'>
                <div class='col-xs-12'>
                    <p class='pull-right'>Todos los derechos reservados</p>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- Modales-->
    <script src='<?php echo base_url();?>js/jquery-1.11.2.min.js'></script>
    <script src='<?php echo base_url();?>js/bootstrap.js'></script>
    <script type='text/javascript'>
        $(document).ready(function(){
            cargarEventos();
            $('#btnAgregar').on('click', function(e){
                e.preventDefault();
                location.href='<?php echo base_url();?>evento/gestion/';
            });
        });
        
        function cargarEventos(){
            $.ajax({
                url : '<?php echo base_url();?>' + 'evento/get',
                datatype : 'JSON',
                type: 'post',
                success: function(result){
                    result = JSON.parse(result);
                    var cadenaHtml = '';
                    for(var i = 0; i < result.length; i++){
                        cadenaHtml += '<div class="col-xs-12 col-sm-6 col-md-4">';
                        cadenaHtml += '<div class="thumbnail">';
                        cadenaHtml += '<img src="' + result[i].urlImagen + '" class="img-responsive img-evento" alt="evento"/>';
                        cadenaHtml += '<div class="caption">';
                        cadenaHtml += '<h3>' + result[i].nombre + '</h3>';
                        cadenaHtml += '<p>' + result[i].fecha + '</p>';
                        cadenaHtml += '<a href="<?php echo base_url();?>evento/informacion/?id=' + result[i].idEvento;
                        cadenaHtml += '" class="btn btn-primary" role="button">Ver más</a>';
                        cadenaHtml += '</div>';
                        cadenaHtml += '</div>';
                        cadenaHtml += '</div>';
                    }
                    $('#lstEventos').html(cadenaHtml);
                }
            });
            
        }
    </script>
</body>