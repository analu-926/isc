<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title>Eventos</title>
    <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>/css/bootstrap.css'/>
    <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>/css/bootflat.css'/>
    <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>/css/main.css'/>
</head>
<body>
    <header>
        <?php $this->load->view('header'); ?>
    </header>
    <section class='container-fluid'>
        <div class='row bg-amarillo'>
            <div class='col-xs-8 col-sm-offset-1 vcenter'>
                <p class='bebas text-center' style='font-size: 3em;'>CONOCE Y PARTICIPA DE TODOS NUESTROS EVENTOS <br>
                    COMPLETAMENTE GRATUITOS
                </p>
            </div><!--
            --><div class='col-xs-4 col-sm-2 vcenter'>
                <img class='img-responsive col-xs-12' src='<?php echo base_url();?>img/eventos_seccion1.png' />
            </div>
        </div>
        <div class='row bg-amarillo'>
            <div class='col-xs-6 col-sm-3'>
                <div class='thumbnail'>
                    <img class='' src=''>
                    <div class='caption'>
                        <h3>Taller de Python</h3>
                        <p>Fecha: 25/05/15</p>
                        <p>
                            <a href='#' class='btn btn-primary'>Ver más</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class='col-xs-6 col-sm-3'>
                <div class='thumbnail'>
                    <img class='' src=''>
                    <div class='caption'>
                        <h3>Taller de Python</h3>
                        <p>Fecha: 25/05/15</p>
                        <p>
                            <a href='#' class='btn btn-primary'>Ver más</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class='col-xs-6 col-sm-3'>
                <div class='thumbnail'>
                    <img class='' src=''>
                    <div class='caption'>
                        <h3>Taller de Python</h3>
                        <p>Fecha: 25/05/15</p>
                        <p>
                            <a href='#' class='btn btn-primary'>Ver más</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class='col-xs-6 col-sm-3'>
                <div class='thumbnail'>
                    <img class='' src=''>
                    <div class='caption'>
                        <h3>Taller de Python</h3>
                        <p>Fecha: 25/05/15</p>
                        <p>
                            <a href='#' class='btn btn-primary'>Ver más</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class='col-xs-6 col-sm-3'>
                <div class='thumbnail'>
                    <img class='' src=''>
                    <div class='caption'>
                        <h3>Taller de Python</h3>
                        <p>Fecha: 25/05/15</p>
                        <p>
                            <a href='#' class='btn btn-primary'>Ver más</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class='col-xs-6 col-sm-3'>
                <div class='thumbnail'>
                    <img class='' src=''>
                    <div class='caption'>
                        <h3>Taller de Python</h3>
                        <p>Fecha: 25/05/15</p>
                        <p>
                            <a href='#' class='btn btn-primary'>Ver más</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class='container-fluid'>
        <?php $this->load->view('footer'); ?>
    </footer>
</body>
</html>
