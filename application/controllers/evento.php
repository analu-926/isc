<?php
class evento extends CI_Controller{
    public function index(){
        $this->load->helper('url');
        $this->load->library('session');
        if($this->session->userdata('usuario')){
            $data['usuario'] = $this->session->userdata('usuario');
            $data['sesion'] = 'true';
        }else{
            $data['sesion'] = 'false';
        }
        $this->load->view('evento', $data);
    }
    
    public function informacion(){
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('evento_model');
        if($this->session->userdata('usuario')){
            $data['usuario'] = $this->session->userdata('usuario');
            $data['sesion'] = 'true';
        }else{
            $data['sesion'] = 'false';
        }
        $data['evento'] = $this->evento_model->get_byId($this->input->get('id'))[0];
        //var_dump($data);
        $this->load->view('informacionE', $data);
    }
    
    public function insert(){
        $this->load->library('session');
        $this->load->model('evento_model');
        echo json_encode($this->evento_model->insert());
    }
    
    public function get(){
        $this->load->model('evento_model');
        echo json_encode($this->evento_model->get());
    }
    
    public function gestion(){
        $this->load->helper('url');
        $this->load->library('session');
        if($this->session->userdata('usuario')){
            $idTipo = $this->session->userdata('usuario')['idTipo'];
            if($idTipo == 2){
                $data['usuario'] = $this->session->userdata('usuario');
                $data['sesion'] = 'true';
                $this->load->view('gestion_evento', $data);
            }else{
                $this->load->view('error_inicia_sesion');
            }
        }else{
            $this->load->view('error_inicia_sesion');
        }        
    }
    
    public function asistencia(){
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('participacion_model');
        if($this->session->userdata('usuario')){
            $data['usuario'] = $this->session->userdata('usuario');
            $data['sesion'] = 'true';
        }else{
            $data['sesion'] = 'false';
        }
        $data['participaciones'] = $this->participacion_model->get_byEvento();
        $this->load->view('asistencia', $data);
    }
}
?>