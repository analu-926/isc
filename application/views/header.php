<nav class='navbar navbar-default navbar-fixed-top navbar-inverse bebas' role='navigation'>
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#menu-principal">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class='navbar-brand' href='<?php echo base_url();?>'>
                <img src='<?php echo base_url();?>img/logo_blanco.svg' class='navbar-img' alt='USAT'/>
            </a>
        </div>
        <div class="collapse navbar-collapse" id='menu-principal'>
            <ul class="nav navbar-nav navbar-right">
                <li><a href='<?php echo base_url();?>welcome/eventos'>EVENTOS</a></li>
                <li><a href='#'>ESCUELA</a></li>
                <li><a href='#'>CONTACTO</a></li>
                <li><button class='btn navbar-btn btn-danger'>Iniciar sesión</button><li>
            </ul>
        </div>
    </div>
</nav>
