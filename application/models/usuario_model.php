<?php
class usuario_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $usuario = array(
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'dni' => $this->input->post('dni'),
            'fechaNac' => $this->input->post('fechaNac'),
            'direccion' => $this->input->post('direccion'),
            'correo' => $this->input->post('correo'),
            'telefono' => $this->input->post('telefono'),
            'movil' => $this->input->post('movil'),
            'pass' => $this->input->post('pass'),
            'carreraPref1' => $this->input->post('carreraPref1'),
            'carreraPref2' => $this->input->post('carreraPref2'),
            'univPref1' => $this->input->post('univPref1'),
            'univPref1' => $this->input->post('univPref1'),
            'estado' => 'G'
        );
         
        $this->db->trans_begin();
        $this->db->insert('usuario', $usuario);
        $this->db->trans_complete();
        
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function short_insert(){
        $this->load->helper('url');
        
        $usuario = array(
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'dni' => $this->input->post('dni'),
            'correo' => $this->input->post('correo'),
            'pass' => $pass = do_hash($this->input->post('pass'), 'md5'),
            'idTipo' => '1',
            'estado' => 'G'
        );
        
        $this->db->trans_begin();
        $this->db->insert('usuario', $usuario);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
        $usuario = array(
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'dni' => $this->input->post('dni'),
            'fechaNac' => $this->input->post('fechaNac'),
            'direccion' => $this->input->post('direccion'),
            'correo' => $this->input->post('correo'),
            'telefono' => $this->input->post('telefono'),
            'movil' => $this->input->post('movil'),
            'pass' => $this->input->post('pass'),
            'carreraPref1' => $this->input->post('carreraPref1'),
            'carreraPref2' => $this->input->post('carreraPref2'),
            'univPref1' => $this->input->post('univPref1'),
            'univPref1' => $this->input->post('univPref1'),
            'estado' => 'G'

        );
        
        $this->db->trans_begin();
        $this->db->where('idUsuario', $this->input->post('idUsuario'));
        $this->db->update('usuario', $usuario);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->db->select('*');
        $this->db->from('usuario');
        $result = $this->db->get();
        return $result->result_array();
        
    }
   
    function get_idUsuario(){
        $this->load->helper('url');
        $this->db->select('idUsuario');
        $this->db->from('usuario');
        $result=$this->db->get();
        return $result->result_array();
    }
    
    function get_byEstado(){
        $this->load->helper('url');
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->like('estado','A'); 
        $result=$this->db->get();
        return $result->result_array();
    }
    
    function get_byCarrera1(){
        $this->load->helper('url');
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('carreraPref1');
        $this->db->like('%sistemas'); 
        $result = $this->db->get();
        return $result->result_array();
    }
    
    function get_byCarrera2(){
        $this->load->helper('url');
        $this->db->select('*');
        $this->db->from('usuario');
         $this->db->where('carreraPref2');
        $this->db->like('%sistemas');
        $result = $this->db->get();
        return $result->result_array();
    }
    
    function get_login(){
        $this->load->helper('url');
        $pass = do_hash($this->input->post('pass'), 'md5');
        $correo = $this->input->post('correo');
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where("correo LIKE '".$correo."' AND pass LIKE '".$pass."'");
        $result = $this->db->get();
        
        return $result->result_array();
    }
}
?>