<?php
class comentario_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $comenta = array(
            'comentario' => $this->input->post('comentario'),
            'tipo' => $this->input->post('tipo')
        );
        
        $this->db->trans_begin();
        $this->db->insert('comenta', $comenta);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
       $comenta = array(
            'comentario' => $this->input->post('comentario'),
            'tipo' => $this->input->post('tipo')
        );
        
        $this->db->trans_begin();
        $this->db->where('idComentario', $this->input->post('idComentario'));
        $this->db->update('comentario', $comentario);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->load->helper('url');   
        $this->db->select('*');
        $this->db->from('comentario');
        $result=$this->db->get();
        return $result->result_array();
        
    }
   
    fuction get_idComentario(){
        $this->load->helper('url');
        $this->db->select('idComentario');
        $this->db->from('comentario');
        $result=$this->db->get();
        return $result->result_array();
}
}
?>