<?php
class hilo_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $hilo = array(
            'pregunta' => $this->input->post('pregunta'),
            'fechaHora' => $this->input->post('fechaHora'),
            'tema' => $this->input->post('tema')
        );
        
        $this->db->trans_begin();
        $this->db->insert('hilo', $hilo);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
       $hilo = array(
            'pregunta' => $this->input->post('pregunta'),
            'fechaHora' => $this->input->post('fechaHora'),
            'tema' => $this->input->post('tema')
        );
        
        $this->db->trans_begin();
        $this->db->where('idHilo', $this->input->post('idHilo'));
        $this->db->update('hilo', $hilo);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->load->helper('url');   
        $this->db->select('*');
        $this->db->from('hilo');
        $result=$this->db->get();
        return $result->result_array();
        
    }
   
    fuction get_idUsuario(){
        $this->load->helper('url');
        $this->db->select('idHilo');
        $this->db->from('hilo');
        $result=$this->db->get();
        return $result->result_array();
}
}
?>