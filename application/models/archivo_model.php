<?php
class archivo_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $archivo = array(
            'nombre' => $this->input->post('nombre'),
            'url' => $this->input->post('url')
        );
        
        $this->db->trans_begin();
        $this->db->insert('archivo', $archivo);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
       $archivo = array(
            'nombre' => $this->input->post('nombre'),
            'url' => $this->input->post('url')
        );
        
        $this->db->trans_begin();
        $this->db->where('idArchivo', $this->input->post('idArchivo'));
        $this->db->update('archivo', $archivo);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->load->helper('url');   
        $this->db->select('*');
        $this->db->from('archivo');
        $result=$this->db->get();
        return $result->result_array();
        
    }
   
    fuction get_idArhivo(){
        $this->load->helper('url');
        $this->db->select('idArchivo');
        $this->db->from('archivo');
        $result=$this->db->get();
        return $result->result_array();
}
}
?>