<div class='row bg-gris-oscuro'>
    <div class='col-xs-12'>
        <h2 class='text-center'>CONTÁCTANOS</h2>
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-sm-6 col-sm-offset-3'>
                    <div class='container-fluid'>
                        <div class='row'>
                            <div class='col-xs-4'>
                                <img class='img-responsive' alt='img' src='<?php echo base_url();?>img/des.png'>
                            </div>
                            <div class='col-xs-4'>
                                <img class='img-responsive' alt='img' src='<?php echo base_url();?>img/soc.png'>
                            </div>
                            <div class='col-xs-4'>
                                <img class='img-responsive img-circle' alt='img' src='<?php echo base_url();?>img/app.png'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-12'>
                    <img src='<?php echo base_url();?>img/logo_blanco.svg' alt='LOGO' class='center-block' style='height:120px;'/>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-12'>
                    <p class='roboto text-center'>
                        Escuela de Ingeniería de Sistemas y Computación - USAT<br>
                        Aynix SRL - Derechos Reservados
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
