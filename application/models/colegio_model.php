<?php
class colegio_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $visita = array(
             'nombre' => $this->input->post('nombre'),
             'direccion' => $this->input->post('direccion'),
             'correo' => $this->input->post('correo'),
             'telefono' => $this->input->post('telefono'),
             'contacto' => $this->input->post('contacto')
        );
        
        $this->db->trans_begin();
        $this->db->insert('colegio', $colegio);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
       $visita = array(
             'nombre' => $this->input->post('nombre'),
             'direccion' => $this->input->post('direccion'),
             'correo' => $this->input->post('correo'),
             'telefono' => $this->input->post('telefono'),
             'contacto' => $this->input->post('contacto')
        );
        
        $this->db->trans_begin();
        $this->db->where('idColegio', $this->input->post('idColegio'));
        $this->db->update('colegio', $colegio);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->load->helper('url');   
        $this->db->select('*');
        $this->db->from('colegio');
        $result=$this->db->get();
        return $result->result_array();
        
    }
   
    fuction get_idColegio(){
        $this->load->helper('url');
        $this->db->select('idColegio');
        $this->db->from('colegio');
        $result=$this->db->get();
        return $result->result_array();
}
    
    
}
?>