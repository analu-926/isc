<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset='utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <title>Sistemas</title>
    <meta name="description" content="Página oficial de la Escuela de Ingeniería de Sistemas - USAT">
    <meta name="author" content="AnaLu Carranza">
     <!-- CSS Code -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.css"/>
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>css/main.css'/>
    <!---link rel="stylesheet" href="<?php echo base_url();?>css/style.css"--->
</head>
<body>
    <header>
        <nav class='navbar navbar-default navbar-fixed-top navbar-inverse' role='navigation'>
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#menu-principal">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class='navbar-brand' href='<?php echo base_url();?>'>ISC</a>
                </div>
                <div class="collapse navbar-collapse" id='menu-principal'>
                    <ul class="nav navbar-nav">
                        <li><a href='<?php echo base_url();?>evento'>Eventos</a></li>
                        <li><a href='#'>Escuela</a></li>
                        <li><a href='#'>Foro</a></li>
                        <li><a href='#'>USAT</a></li>
                        <li><a href='#'>Contáctanos</a></li>
                    </ul>
                    <ul class='nav navbar-nav navbar-right'>
                        <li><a href='<?php echo base_url();?>usuario/logueo'>Iniciar sesión</a></li>
                    </ul>
                </div>   
            </div>  
        </nav> 
    </header>
    <section class="container">
        <div class='row'>
            <div class='col-sm-6'>
                <form class='form-signin form-pequeno' id='frmSesion'>
                    <h2 class='form-signin-heading'>Inicio de sesión</h2>
                    <label for='txtCorreo' class='sr-only'>Email</label>
                    <input type='email' id='txtCorreo' class='form-control input-top' placeholder='Correo' required autofocus/>
                    <label for='txtPass' class='sr-only'>Contraseña</label>
                    <input type='password' id='txtPass' class='form-control input-bottom' placeholder='Contraseña' required/>
                    <button class='btn btn-primary btn-block' type='submit' id='btnIngresar'>Ingresar</button>
                    <a href='#' class='text-center'>Olvidé mi contraseña</a>
                    <div id='alert'></div>
                </form>
            </div>
            <div class='col-sm-6'>
                <div class='form-pequeno'>
                    <form class='form-signin' id='frmRegistro'>
                        <h2 class='form-signing-heading'>Regístrate</h2>
                        <input type='text' id='txtNombre' class='form-control input-top' placeholder='Nombre' required/>
                        <input type='text' id='txtApellidos' class='form-control input-medium' placeholder='Apellidos' required/>
                        <input type='text' id='txtDni' class='form-control input-medium' placeholder='DNI' required/>
                        <input type='email' id='txtCorreoR' class='form-control input-medium' placeholder='ejemplo@correo.com' required />
                        <input type='password' id='txtPassR' class='form-control input-bottom' placeholder='Contraseña' required/>
                        <button type='submit' class='btn btn-primary btn-block' id='btnRegistrar'>Registrarse</button>
                        <div id='alertR'></div>
                    </form>
                </div>
            </div>
        </div>
    </section>    
    <!-- Modales-->
    <script src='<?php echo base_url();?>js/jquery-1.11.2.min.js'></script>
    <script src='<?php echo base_url();?>js/bootstrap.js'></script>
    <script type='text/javascript'>
        $(document).ready(function(){
            $('#frmSesion').on('submit', function(e){
                e.preventDefault();
                var correo = $('#txtCorreo').val();
                var pass = $('#txtPass').val();
                $.ajax({
                    url: '<?php echo base_url();?>usuario/login',
                    type: 'post',
                    datatype: 'json',
                    data:{
                        correo : correo,
                        pass : pass
                    },
                    success: function(result){
                        var result = JSON.parse(result);
                        if(result == 'true'){
                            location.href = '<?php echo base_url();?>evento';
                        }else{
                            var cadena = '<div class="alert alert-danger alert-dismissible fade in" role="alert">';
                            cadena += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                            cadena += '<span aria-hidden="true">x</span>';
                            cadena += '</button>';
                            cadena += '<h4>Error de inicio</h4>';
                            cadena += '<p>Usuario o contraseña incorrectos, por favor intente de nuevo</p>';
                            cadena += '</div>';
                            $('#alert').html(cadena);
                        }
                    }
                    
                });
            });
            
            $('#frmRegistro').on('submit', function(e){
                e.preventDefault();
                var nombre = $('#txtNombre').val();
                var apellido = $('#txtApellidos').val();
                var dni = $('#txtDni').val();
                var correo = $('#txtCorreoR').val();
                var pass = $('#txtPassR').val();
                $.ajax({
                    url: '<?php echo base_url();?>usuario/short_insert',
                    type: 'post',
                    datatype: 'json',
                    data:{
                        nombre : nombre,
                        apellido : apellido,
                        dni: dni,
                        correo : correo,
                        pass : pass
                    },
                    success: function(result){
                        var result = JSON.parse(result);
                        if(result != 'true'){
                            var cadena = '<div class="alert alert-danger alert-dismissible fade in" role="alert">';
                            cadena += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                            cadena += '<span aria-hidden="true">x</span>';
                            cadena += '</button>';
                            cadena += '<h4>Error en el registro</h4>';
                            cadena += '<p>Lo sentimos, ha ocurrido un error al registrar los datos, vuelve a intentarlo más tarde.</p>';
                            cadena += '</div>';
                            $('#alertR').html(cadena);
                        }else{
                            var cadena = '<div class="alert alert-success alert-dismissible fade in" role="alert">';
                            cadena += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                            cadena += '<span aria-hidden="true">x</span>';
                            cadena += '</button>';
                            cadena += '<h4>Registrado correctamente</h4>';
                            cadena += '<p>Ya casi está listo, ya puedes iniciar sesión y disfrutar de los eventos.</p>';
                            cadena += '</div>';
                            $('#alertR').html(cadena);
                        }
                    }
                    
                });
            });
        });
    </script>
</body>