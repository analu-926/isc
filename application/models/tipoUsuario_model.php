<?php
class tipoUsuario_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $tipoUsuario = array(
            'tipo' => $this->input->post('tipo')
        );
        
        $this->db->trans_begin();
        $this->db->insert('tipoUsuario', $tipoUsuario);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
       $tipoUsuario = array(
           'tipo' => $this->input->post('tipo')
        );
        
        $this->db->trans_begin();
        $this->db->where('idTipo', $this->input->post('idTipo'));
        $this->db->update('tipoUsuario', $tipoUsuario);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->load->helper('url');   
        $this->db->select('*');
        $this->db->from('tipoUsuario');
        $result=$this->db->get();
        return $result->result_array();    
    }
}
?>