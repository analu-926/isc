<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset='utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <title>Sistemas</title>
    <meta name="description" content="Página oficial de la Escuela de Ingeniería de Sistemas - USAT">
    <meta name="author" content="AnaLu Carranza">
     <!-- CSS Code -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin">
    <!---link rel="stylesheet" href="<?php echo base_url();?>css/style.css"-->
    <link rel='stylesheet' type='text/css' href='css/main.css'/>
</head>
<body>
    <header>
        <nav class='navbar navbar-default navbar-fixed-top navbar-inverse' role='navigation'>
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#menu-principal">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class='navbar-brand' href='<?php echo base_url();?>'>ISC</a>
                </div>
                <div class="collapse navbar-collapse" id='menu-principal'>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href='<?php echo base_url();?>evento'>Eventos</a></li>
                        <li><a href='#'>Escuela</a></li>
                        <li><a href='#'>Foro</a></li>
                        <li><a href='#'>USAT</a></li>
                        <li><a href='#'>Contáctanos</a></li>
                    </ul>
                    <ul class='nav navbar-nav navbar-right'>
                        <li>
                            <?php if($sesion == 'true'){?>
                            <a href='<?php echo base_url();?>usuario/logout'><?php echo $usuario['correo'];?>[Cerrar]</a>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <section class="container">
        <h1 class='text-center'>Nuevo Evento</h1>
        <div class='row'>
            <div class='col-xs-12'>
                <form class='form-horizontal' id='frmRegistroEvento'>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtNombre'>Nombre</label>
                        <div class='col-xs-9'>
                            <input type='text' id='txtNombre' class='form-control input-sm' placeholder='Nombre' required/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtTema'>Tema</label>
                        <div class='col-xs-9'>
                            <input type='text' id='txtTema' class='form-control input-sm' placeholder='Tema' required/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtDescripcion'>Descripción</label>
                        <div class='col-xs-9'>
                            <textArea id='txtDescripcion' class='form-control input-sm' rows='5' placeholder='Descripción' required></textArea>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtfecha'>Fecha</label>
                        <div class='col-xs-2'>
                            <input type='date' id='txtFecha' class='form-control input-sm' required/>
                        </div>
                        <label class='control-label col-xs-1' for='txtHoraInicio'>Inicio</label>
                        <div class='col-xs-2'>
                            <input type='time' id='txtHoraInicio' class='form-control input-sm' required/>
                        </div>
                        <label class='control-label col-xs-1' for='txtHoraFin'>Fin</label>
                        <div class='col-xs-2'>
                            <input type='time' id='txtHoraFin' class='form-control input-sm' required/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtLugar'>Lugar</label>
                        <div class='col-xs-9'>
                            <input type='text' id='txtLugar' class='form-control input-sm' placeholder='Lugar' required>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtCoordenadas'>Coordenadas</label>
                        <div class='col-xs-2'>
                            <input type='text' id='txtCoordenadas' class='form-control input-sm' placeholder='0.00000,0.00000'/>
                        </div>
                    </div>
                    <!--div class='form-group'>
                        <label class='control-label col-xs-2' for='flImagen'>Imagen</label>
                        <div class='col-xs-4'>
                            <input type='file' id='flImagen' class='form-control input-sm'/>
                        </div>
                    </div-->
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='txtUrlImagen'>Imagen</label>
                        <div class='col-xs-4'>
                            <input type='url' id='txtUrlImagen' class='form-control input-sm' placeholder='http://www.camila.soy' required/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='url1'>Url1</label>
                        <div class='col-xs-4'>
                            <input type='url' id='url1' class='form-control input-sm' placeholder='http://www.ejemplo.soy' required/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-xs-2' for='url2'>Url2</label>
                        <div class='col-xs-4'>
                            <input type='url' id='url2' class='form-control input-sm' placeholder='http://www.ejemplo2.soy' required/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <div class='col-xs-offset-2 col-xs-1'>
                            <button type='submit' class='btn btn-primary'>Aceptar</button>
                        </div>
                        <div class='col-xs-1'>
                            <button type='button' class='btn btn-danger'>Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer class="footer">
        <?php $this->load->view('footer'); ?>
    </footer>
</body>
