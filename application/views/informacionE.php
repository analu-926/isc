<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset='utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <title>Sistemas</title>
    <meta name="description" content="Página oficial de la Escuela de Ingeniería de Sistemas - USAT">
    <meta name="author" content="AnaLu Carranza">
     <!-- CSS Code -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.css"/>
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin">
    <!---link rel="stylesheet" href="<?php echo base_url();?>css/style.css"--->
    <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>css/main.css'/>
</head>
<body>
    <header>
        <nav class='navbar navbar-default navbar-fixed-top navbar-inverse' role='navigation'>
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#menu-principal">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class='navbar-brand' href='<?php echo base_url();?>'>ISC</a>
                </div>
                <div class="collapse navbar-collapse" id='menu-principal'>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href='<?php echo base_url();?>evento'>Eventos</a></li>
                        <li><a href='#'>Escuela</a></li>
                        <li><a href='#'>Foro</a></li>
                        <li><a href='#'>USAT</a></li>
                        <li><a href='#'>Contáctanos</a></li>
                    </ul>
                    <ul class='nav navbar-nav navbar-right'>
                        <li>
                            <?php if($sesion == 'true' && $usuario['idTipo'] == 2){?>
                            <button type='button' class='btn btn-primary navbar-btn' id='btnEditar'>Editar</button>
                            <?php }?>
                        </li>
                        <li>
                            <?php if($sesion == 'false') {?>
                            <a href='<?php echo base_url();?>usuario/logueo'>Iniciar sesión</a>
                            <?php }else{?>
                            <a href='<?php echo base_url();?>usuario/logout'><?php echo $usuario['correo'];?>[Cerrar]</a>
                            <?php } ?>
                        </li>
                    </ul>
                </div>   
            </div>  
        </nav> 
    </header>
    <section class="container">
        <div class="row">
            <div class='col-xs-12'>
                <h1><?php echo $evento['nombre'];?></h1>
                <h2><?php echo $evento['tema'];?></h2>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-12'>
                <p><?php echo $evento['descripcion'];?></p> 
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">
                <input type='hidden' id='txtIdEvento' value='<?php echo $evento['idEvento'];?>'/>
                <?php
                if( $sesion == 'true' && $usuario['idTipo'] == 2){
                ?>
                <a href='<?php echo base_url();?>evento/asistencia?id=<?php echo $evento['idEvento'];?>' class='btn btn-success'>Asistencia</a>
                <?php
                }else{
                ?>
                <button type="button" class="btn btn-success" id='btnRegistro'>Regístrate</button>
                <?php
                }
                ?>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class='row'>
                <div class='col-xs-12'>
                    <p class='pull-right'>Todos los derechos reservados</p>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- MODALES-->
    <div class='modal fade' id='mdlMensaje'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    <h4>Mensaje</h4>
                </div>
                <div class='modal-body' id='mdlBody'>                    
                </div>
            </div>
        </div>
    </div>
    <script src='<?php echo base_url();?>js/jquery-1.11.2.min.js'></script>
    <script src='<?php echo base_url();?>js/bootstrap.js'></script>
    <script type='text/javascript'>
        $(document).ready(function(){
            $('#btnRegistro').on('click', function(e){
                e.preventDefault();
                var idEvento = $('#txtIdEvento').val();
                    
                $.ajax({
                    url : '<?php echo base_url();?>usuario/participar',
                    type : 'post',
                    datatype : 'json',
                    data: {
                        idEvento : idEvento
                    },
                    success: function(result){
                        var result = JSON.parse(result);
                        var cadenaHtml = '';
                        if(result == 'true'){
                            cadenaHtml += '<p>Te has registrado en el evento.</p>';                            
                        }else{
                            cadenaHtml += '<p>Debes iniciar sesión para poder registrarte en el evento.</p>';
                            cadenaHtml += '<a href="<?php echo base_url();?>usuario/logueo">Iniciar Sesión</a>';
                        }
                        $('#mdlBody').html(cadenaHtml);
                        $('#mdlMensaje').modal('show');
                    }
                });
            });
            $('#btnEditar').on('click', function(e){
                e.preventDefault();
                location.href='<?php echo base_url();?>evento/gestion/?id=<?php echo $evento['idEvento'];?>';
            });
        });
                          
    </script>
</body>