<?php
class participacion_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert($idUsuario){
        $this->load->helper('url');
        
        $participacion = array(
            'idUsuario' => $idUsuario,
            'idEvento' => $this->input->post('idEvento'),
            'asistencia' => 'G'
        );
        
        $this->db->trans_begin();
        $this->db->insert('participacion', $participacion);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get_byEvento(){
        $this->load->helper('url');
        
        $this->db->select('usuario.*');
        $this->db->from('participacion inner join usuario on usuario.idUsuario = participacion.idUsuario');
        $this->db->where('idEvento', $this->input->get('id'));
        $result = $this->db->get();
        
        return $result->result_array();
    }
}
?>