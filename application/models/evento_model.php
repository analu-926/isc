<?php
class evento_model extends CI_Model{
    function __construct(){
        $this->load->database();
    }
    
    function insert(){
        $this->load->helper('url');
        
        $evento = array(
            'nombre' => $this->input->post('nombre'),
            'tema' => $this->input->post('tema'),
            'descripcion' => $this->input->post('descripcion'),
            'fecha' => $this->input->post('fecha'),
            'horaInicio' => $this->input->post('horaInicio'),
            'horaFin' => $this->input->post('horaFin'),
            'lugar' => $this->input->post('lugar'),
            'coordenadas' => $this->input->post('coordenadas'),
            //'urlImagen' => $this->input->post('urlImagen'),
            'url1' => $this->input->post('url1'),
            'url2' => $this->input->post('url2'),
            'estado' => 'G'
        );
        
        $this->db->trans_begin();
        $this->db->insert('evento', $evento);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function update(){
        $this->load->helper('url');
        
        $evento = array(
            'nombre' => $this->input->post('nombre'),
            'tema' => $this->input->post('tema'),
            'descripcion' => $this->input->post('descripcion'),
            'fecha' => $this->input->post('fecha'),
            'horaInicio' => $this->input->post('horaInicio'),
            'horaFin' => $this->input->post('horaFin'),
            'lugar' => $this->input->post('lugar'),
            'coordenadas' => $this->input->post('coordenadas'),
            'urlImagen' => $this->input->post('urlImagen'),
            'url1' => $this->input->post('url1'),
            'url2' => $this->input->post('url2'),
            'estado' => 'G'
        );
        
        $this->db->trans_begin();
        $this->db->where('idEvento', $this->input->post('idEvento'));
        $this->db->update('evento', $evento);
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE)
            return 'false';
        return 'true';
    }
    
    function get(){
        $this->load->helper('url');   
        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('estado','G');
        $result = $this->db->get();
        return $result->result_array();
        
    }
    
    function get_byId($id){
        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('idEvento', $id);
        $result = $this->db->get();
        
        return $result->result_array();
    }
    
    function get_idEvento(){
        $this->load->helper('url');
        $this->db->select('idEvento');
        $this->db->from('evento');
        $result=$this->db->get();
        return $result->result_array();
    }
    
    function get_byTema(){
        $this->load->helper('url');
        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('tema');
        $this->db->like('%%'); 
        $result=$this->db->get();
        return $result->result_array();
    }    
}
?>