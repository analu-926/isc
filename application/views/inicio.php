<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset='utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <title>Sistemas</title>
    <meta name="description" content="Página oficial de la Escuela de Ingeniería de Sistemas - USAT">
    <meta name="author" content="AnaLu Carranza">
    <!-- CSS Code -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootflat.css"/>
    <link rel='stylesheet' type='text/css' href='css/main.css'/>
</head>
<body>
    <header>
        <?php $this->load->view('header'); ?>
    </header>
    <section class="container-fluid">
        <div class='row'>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src='<?php echo base_url();?>img/uss.png' class='img-responsive' alt="">
                        <div class="carousel-caption">

                        </div>
                    </div>

                    <div class="item">
                        <img src="<?php echo base_url();?>img/isc.jpg" alt="Portada2">
                        <div class="carousel-caption">
                            Participa de nuestros eventos
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url();?>img/us.png" alt="Portada2">
                        <div class="carousel-caption">
                            ...
                        </div>
                    </div>

                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class='row bg-rojo'>
            <div class='col-sm-7 vcenter'>
                <img class='img-responsive col-xs-12' src='img/inicio_laptop.png' alt='laptop'/>
            </div><!--
            --><div class='col-sm-5 vcenter text-center' id='inicio-seccion2'>
                <p class='roboto'>Ingeniería de Sistemas y Computación</p>
                <h2>¡CONÓCENOS!</h2>
            </div>
        </div>
        <div class='row bg-gris'>
            <div class='col-sm-5 vcenter text-center' id='inicio-seccion2'>
                <h2>PUEDO TRABAJAR COMO...</h2>
                <ul class='roboto lst-trabajos'>
                    <li>Auditor</li>
                    <li>Auditor</li>
                    <li>Auditor</li>
                    <li>Auditor</li>
                </ul>
            </div><!--
            --><div class='col-sm-7 vcenter'>
                <img class='img-responsive col-xs-12' src='img/inicio_seccion2.png' alt='laptop'/>
            </div>
        </div>
        <div class='row bg-azul'>
            <div class='col-xs-12'>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-xs-2 col-xs-offset-2'>
                            <img src='' alt='IMG'/>
                        </div>
                        <div class='col-xs-2 '>
                            <img src='' alt='IMG'/>
                        </div>
                        <div class='col-xs-2 '>
                            <img src='' alt='IMG'/>
                        </div>
                        <div class='col-xs-2 '>
                            <img src='' alt='IMG'/>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-xs-12'>
                <h2 class='text-center'>¿POR QUÉ ELEGIR USAT?</h2>
            </div>
        </div>
        <div class='row bg-amarillo'>
            <div class='col-sm-4 col-sm-offset-4'>
                <h2 class='text-center'>¡REGÍSTRATE YA!</h2>
            </div>
        </div>
    </section>
    <footer class='container-fluid'>
        <?php $this->load->view('footer'); ?>
    </footer>
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
